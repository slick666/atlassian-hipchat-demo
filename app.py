"""Simple api application to demonstrate parser functionality"""

# Standard Library
import json

# Packages
from flask import Flask, request

# Local Software
from input_parser import parse_data

app = Flask(__name__)


@app.route('/', methods=['POST'])
def parse_string():
    return json.dumps(
        parse_data(
            str(request.data, 'utf-8')
        )
    )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
