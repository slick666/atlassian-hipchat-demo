"""All the unit tests associated with the input_parser file"""

import unittest
from unittest.mock import MagicMock, patch

from input_parser import parse_mentions, parse_emotocons, get_title, parse_links


class Mentions(unittest.TestCase):
    """
    mentions - A way to mention a user. Always starts with an '@' and ends
    when hitting a non-word character.
    (https://confluence.atlassian.com/hipchat/get-teammates-attention-744328217.html)
    """
    def test_given(self):
        """
        Test the given examples in the documentation
        :return:
        """
        self.assertListEqual(
            parse_mentions('@chris you around?'),
            ['chris']
        )
        self.assertListEqual(
            parse_mentions('@bob @john (success) such a cool feature; '
                           'https://twitter.com/jdorfman/status/430511497475670016'),
            ['bob', 'john']
        )

    def test_double(self):
        """
        Test what happens when a double '@@' is used
        :return:
        """
        self.assertListEqual(
            parse_mentions("This isn't a valid user @@"),
            []
        )
        self.assertListEqual(
            parse_mentions("This perhaps should be @@user"),
            ['user']
        )

    def test_gap(self):
        self.assertListEqual(
            parse_mentions("This user contains one, @thing1@thing2"),
            ['thing1', 'thing2']
        )


class Emotocons(unittest.TestCase):
    """
    Emoticons - For this exercise, you only need to consider 'custom'
    emoticons which are alphanumeric strings, no longer than 15 characters,
    contained in parenthesis. You can assume that anything matching this
    format is an emoticon. (https://www.hipchat.com/emoticons)
    """

    def test_given(self):
        self.assertListEqual(
            parse_emotocons("Good morning! (megusta) (coffee)"),
            ['megusta', 'coffee']
        )

        self.assertListEqual(
            parse_emotocons('@bob @john (success) such a cool feature; '
                            'https://twitter.com/jdorfman/status/430511497475670016'),
            ['success']
        )


class GetTitle(unittest.TestCase):
    """
    Sub function of the parse_links that fetches the given url and returns the title from that url
    """

    def test_given(self):
        """
        NONE: Depends upon the url 'https://twitter.com/jdorfman/status/430511497475670016'
        :return:
        """
        self.assertEqual(
            get_title('https://twitter.com/jdorfman/status/430511497475670016'),
            ('Justin Dorfman; on Twitter: &quot;nice @littlebigdetail from '
             '@HipChat (shows hex colors when pasted in chat). '
             'http://t.co/7cI6Gjy5pq&quot;')
        )


class Links(unittest.TestCase):
    """
    Links - Any URLs contained in the message, along with the page's title.
    """

    @patch('input_parser.get_title')
    def test_given(self, mock_get_title):
        mock_get_title.return_value = '2016 Rio Olympic Games | NBC Olympics'

        self.assertListEqual(
            parse_links('Olympics are starting soon; http://www.nbcolympics.com'),
            [
                {
                    "url": "http://www.nbcolympics.com",
                    "title": "2016 Rio Olympic Games | NBC Olympics"
                }
            ]
        )

        mock_get_title.return_value = (
            "Justin Dorfman on Twitter: &quot;nice @littlebigdetail "
            "from @HipChat (shows hex colors when pasted in chat). "
            "http://t.co/7cI6Gjy5pq&quot;"
        )

        self.assertListEqual(
            parse_links('@bob @john (success) such a cool feature; '
                        'https://twitter.com/jdorfman/status/430511497475670016'),
            [
                {
                    "url": "https://twitter.com/jdorfman/status/430511497475670016",
                    "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail "
                             "from @HipChat (shows hex colors when pasted in chat). "
                             "http://t.co/7cI6Gjy5pq&quot;"
                }
            ]
        )


if __name__ == '__main__':
    unittest.main()
