"""
parser.py contains all the logic associated with parsing input strings
"""

# Standard library imports
import re

# Community packages
import httplib2
from urlextract import URLExtract


def get_title(url):
    """
    simple function that returns the page title as a string from the given url
    :param url: given url
    :return: string of the page title
    """
    http = httplib2.Http()
    content = http.request(url)[1]

    title = re.split(
        pattern='(<title>)|(<\/title>)',
        string=content.decode(),
    )[3]

    return title


def parse_mentions(input_string):
    """
    Given an input string find all mentions.

    mentions are a way to mention a user. Always starts with an '@' and ends
    when hitting a non-word character.
    :param input_string: given string object
    :return: list or matching substrings
    """
    regex_pattern = '\@\w+'
    return [
        elem.strip('@')
        for elem in re.findall(
            pattern=regex_pattern,
            string=input_string,
        )
    ]


def parse_emotocons(input_string):
    """
    Given an input string find all emotocons.

    For this exercise, you only need to consider 'custom' emoticons which are
    alphanumeric strings, no longer than 15 characters, contained in
    parenthesis. You can assume that anything matching this format is an
    emoticon. (https://www.hipchat.com/emoticons)

    :param input_string: given string object
    :return: list of matching substrings
    """
    regex_pattern = '[(][a-zA-Z0-9]{1,15}[)]'
    return [
        elem.strip('()')
            for elem in re.findall(
            pattern=regex_pattern,
            string=input_string,
        )
    ]


def parse_links(input_string):
    """
    Given an input string find all the links present

    Links - Any URLs contained in the message, along with the page's title.

    :param input_string: given string
    :return: list of dictionaries
    """
    extractor = URLExtract()
    urls = extractor.find_urls(input_string)
    return [
        {
            'url': url,
            'title': get_title(url)
        }
        for url in urls
    ]


def parse_data(input_string):
    """
    Given the input string parse all the potential data types and return the result
    :param input_string: given string
    :return: dictionary with parsed data
    """

    return {
        "mentions": parse_mentions(input_string),
        "emotocons": parse_emotocons(input_string),
        "links": parse_links(input_string),
    }


if __name__ == '__main__':
    import sys
    import pprint

    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(parse_data(sys.argv[1]))
