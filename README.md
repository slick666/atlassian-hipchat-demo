# Rackspace Demo
This project is the answer for the "take-home coding exercise" for Landon Jurgens. Submitted to Atlassian on July 1st 2017.

## Setup and Use
The solution was solved using [Python](https://www.python.org/downloads/release/python-353/) Python 3.5.3 on Ubuntu Linux 17.04, and testing was limited this environment. The specific version string from the Python interceptor is `Python 3.5.3 (default, Jan 19 2017, 14:11:04) 
[GCC 6.3.0 20170118] on linux`.

### Installation
The installation process is very basic for this example. 

1. Have a system with Python3 installed.
2. Clone or download the repository
3. Navigate to the repository subdirectory
4. Install the required python packages `pip install -r requirements.txt`

### Testing

To run the unit tests for the question, and execute the `input_parse_tests.py` file. Example output:

    $ python input_parse_tests.py
    .....
    ----------------------------------------------------------------------
    Ran 6 tests in 0.619s

    OK

### Execution

To execute the api as a test server on our local instance execute Python with the app.py as the argument

    $ python app.py 
    * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
    * Restarting with stat
    * Debugger is active!
    * Debugger PIN: 274-424-619

This will give you a local api running on port 5000 that you can post to

# Explanation

### Given Question

We'd like you to complete a take-home coding exercise.  This exercise is not meant to be tricky or complex; however, it does represent a typical problem faced by the HipChat Engineering team.  Here are a few things to keep in mind as you work through it:
Hi Landon,
* The position is for a role on the Platform team.  As a platform team, we work in Python.  If you are not comfortable working in Python, we encourage you to code your solution using the language of your choice. We are much more interested in how you solve the problem than we are in how much of a new language you can learn in a few hours. 
* Please only spend 2 hours on this exercise.   Treat this as if you're a member of the HipChat Engineering team and are solving it as part of your responsibilities there.
* Be thorough and take the opportunity to show the HipChat Engineering team that you've got technical chops.
* Using frameworks and libraries is acceptable. We are looking for how you would solve a problem like this on the job. If that involves bringing in libraries then do so, and even better, tell us why you made the choice.
  
When you think it's ready for prime time, push your work to a public repo on Bitbucket or Github and send us a link.
  
Now, for the coding exercise...
Please write a RESTful API that takes a chat message string as input and returns a JSON object containing information about its contents as described below.
  
Your service should parse the following data from the input:
1. mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (https://confluence.atlassian.com/hipchat/get-teammates-attention-744328217.html)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)
3. Links - Any URLs contained in the message, along with the page's title.
  
The response should be a JSON object containing arrays of all matches parsed from the input string.
For example, calling your function with the following inputs should result in the corresponding return values.
```
Input: "@chris you around?"
Return:
{
  "mentions": [
    "chris"
  ]
}
```

```
Input: "Good morning! (megusta) (coffee)"
Return:
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}
```

```
Input: "Olympics are starting soon; http://www.nbcolympics.com"
Return:
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "2016 Rio Olympic Games | NBC Olympics"
    }
  ]
}
```

```
Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
Return:
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
    }
  ]
}
```
Good luck!


### Solution

Since no version of python was specified Python 3 was chosen by default. All core functionality is found in the `input_parser.py` file. Each of the three categories are given a separate function with a thin root function that calls all three with the same argument. All of this is wrapped in a very thing Flask application exposing just the POST functionality.

### Results

    $ curl -i -H "Content-Type:text/plain" -X POST -d '\@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016' http://localhost:5000/
    HTTP/1.0 200 OK
    Content-Type: text/html; charset=utf-8
    Content-Length: 284
    Server: Werkzeug/0.12.2 Python/3.5.3
    Date: Sat, 01 Jul 2017 19:58:18 GMT
    
    {"links": [{"url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Justin Dorfman; on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"}], "emotocons": ["success"], "mentions": ["bob", "john"]}
